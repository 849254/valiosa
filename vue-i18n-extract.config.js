
module.exports = {
  vueFilesPath: './src/**/*.vue', // The Vue.js file(s) you want to extract i18n strings from. It can be a path to a folder or to a file. It accepts glob patterns. (ex. *, ?, (pattern|pattern|pattern)
  languageFilesPath: './src/i18n/**/*.json', // The language file (s) you want to compare your Vue.js file (s) to.It can be a path to a folder or to a file.It accepts glob patterns (ex. *, ?, (pattern | pattern | pattern)
  options: {
    output: false,
    add: true,
    dynamic: false
  }
}
