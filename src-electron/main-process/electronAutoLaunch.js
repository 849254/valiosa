/* eslint-disable indent */
const AutoLaunch = require('auto-launch')
let autoLauncher

exports.init = (name) => {
    autoLauncher = new AutoLaunch({
        name: name
    })
}

exports.enable = () => {
    autoLauncher.isEnabled().then(function (isEnabled) {
        if (isEnabled) return
        autoLauncher.enable()
    }).catch(function (err) {
        throw err
    })
}
