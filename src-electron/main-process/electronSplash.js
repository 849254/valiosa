/* eslint-disable indent */
import { BrowserWindow } from 'electron'
import { resolve } from 'app-root-path'

// const { width, height } = electron.screen.getPrimaryDisplay().workAreaSize
let splash
exports.init = () => {
    splash = new BrowserWindow({ width: 602, height: 358, transparent: true, frame: false, alwaysOnTop: true })
}

exports.load = () => {
    splash.loadURL(`file://${resolve('../icons/splash.jpg')}`)
    setTimeout(() => { }, 5000)
}

exports.end = () => {
    splash.destroy()
}
