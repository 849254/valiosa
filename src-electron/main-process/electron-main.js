import { app, BrowserWindow, nativeTheme, Menu, Tray } from 'electron'
const electron = require('electron')
const { autoUpdater } = require('electron-updater')
const AutoLaunch = require('auto-launch')
import { resolve } from 'app-root-path'

// const path = require('path')
let x = 0
let updateInterval
async function persistUpdateCheck () {
  updateInterval = setInterval(() => {
    autoUpdater.checkForUpdatesAndNotify()
    x += 1
    console.log('updating', x)
  }, 10000)
}
const autoLauncher = new AutoLaunch({
  name: 'valiosa_f'
})

try {
  if (process.platform === 'win32' && nativeTheme.shouldUseDarkColors === true) {
    require('fs').unlinkSync(require('path').join(app.getPath('userData'), 'DevTools Extensions'))
  }
} catch (_) { }

/**
 * Set `__statics` path to static files in production;
 * The reason we are setting it here is that the path needs to be evaluated at runtime
 */
if (process.env.PROD) {
  global.__statics = __dirname
}

let mainWindow, splash

function createWindow () {
  /**
   * Initial window options
   */
  const { width, height } = electron.screen.getPrimaryDisplay().workAreaSize
  splash = new BrowserWindow({ width: 602, height: 358, transparent: true, frame: false, alwaysOnTop: true })
  splash.loadURL(`file://${resolve('../icons/splash.jpg')}`)
  setTimeout(() => {
    mainWindow = new BrowserWindow({
      width: width,
      height: height,
      useContentSize: false,
      webPreferences: {
        // Change from /quasar.conf.js > electron > nodeIntegration;
        // More info: https://quasar.dev/quasar-cli/developing-electron-apps/node-integration
        nodeIntegration: process.env.QUASAR_NODE_INTEGRATION,
        nodeIntegrationInWorker: process.env.QUASAR_NODE_INTEGRATION

        // More info: /quasar-cli/developing-electron-apps/electron-preload-script
        // preload: path.resolve(__dirname, 'electron-preload.js')
      }
    })
    splash.destroy()
    mainWindow.loadURL(process.env.APP_URL)
    mainWindow.on('close', function (event) {
      if (!app.isQuiting) {
        event.preventDefault()
        mainWindow.hide()
      }
      return false
    })
    mainWindow.once('ready-to-show', () => {
      persistUpdateCheck()
    })

    autoUpdater.on('update-available', () => {
      clearInterval(updateInterval)
      mainWindow.webContents.send('update_available')
    })

    autoUpdater.on('update-downloaded', () => {
      mainWindow.webContents.send('update_downloaded')
    })
  }, 5000)
}
const gotTheLock = app.requestSingleInstanceLock()

if (!gotTheLock) {
  app.quit()
} else {
  app.on('second-instance', (event, commandLine, workingDirectory) => {
    // Someone tried to run a second instance, we should focus our window.
    if (mainWindow) {
      if (mainWindow.isMinimized()) mainWindow.restore()
      mainWindow.focus()
    }
  })

  // Create myWindow, load the rest of the app, etc...
  app.on('ready', () => {
  })
}
let tray = null
app.on('ready', () => {
  //    icon: path.join(__dirname, 'relative/path/to/myIcon.ico')

  tray = new Tray(resolve('../icons/icon.png'))
  const contextMenu = Menu.buildFromTemplate([
    {
      label: 'Show App',
      click: function () {
        mainWindow.show()
      }
    },
    {
      label: 'Quit',
      click: function () {
        app.isQuiting = true
        app.quit()
      }
    }
  ])
  tray.setToolTip('This is my application.')
  tray.setContextMenu(contextMenu)
  createWindow()
})
// Checking if autoLaunch is enabled, if not then enabling it.
autoLauncher.isEnabled().then(function (isEnabled) {
  if (isEnabled) return
  autoLauncher.enable()
}).catch(function (err) {
  throw err
})
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})
