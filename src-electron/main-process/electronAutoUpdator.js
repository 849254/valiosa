/* eslint-disable indent */
const { autoUpdater } = require('electron-updater')
let x = 0
let updateInterval
async function persistUpdateCheck () {
    updateInterval = setInterval(() => {
        autoUpdater.checkForUpdatesAndNotify()
        x += 1
        console.log('updating', x)
    }, 10000)
}
exports.init = () => {
    persistUpdateCheck()
}
exports.implement = (mainWindow) => {
    autoUpdater.on('update-available', () => {
        clearInterval(updateInterval)
        mainWindow.webContents.send('update_available')
    })

    autoUpdater.on('update-downloaded', () => {
        mainWindow.webContents.send('update_downloaded')
    })
}
