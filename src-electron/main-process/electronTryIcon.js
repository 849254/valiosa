/* eslint-disable indent */
import { app, Menu, Tray } from 'electron'
import { resolve } from 'app-root-path'

// close persist to try
exports.init = (mainWindow, tooltip = 'our app') => {
    const tray = new Tray(resolve('../icons/icon.png'))
    const contextMenu = Menu.buildFromTemplate([
        {
            label: 'Show App',
            click: function () {
                mainWindow.show()
            }
        },
        {
            label: 'Quit',
            click: function () {
                app.isQuiting = true
                app.quit()
            }
        }
    ])
    tray.setToolTip(tooltip)
    tray.setContextMenu(contextMenu)
}
