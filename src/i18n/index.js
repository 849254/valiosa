import enUS from './en-us'
import ar from './ar'

export default {
  'en-US': enUS,
  ar
}
