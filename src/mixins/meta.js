export default {
  methods: {
    setMeta (meta) {
      this.pmeta = meta
    },
    setTitle (t) {
      this.pmeta.title = t
    },
    setTitleTemplate (tt) {
      this.pmeta.titleTemplate = tt
    },
    setTitlesByRoutes (titleRoutes) {
      this.titleRoutes = titleRoutes
    },
    cw (word) {
      return word.charAt(0).toUpperCase() + word.slice(1)
    }
  },
  updated () {
    /* const self = this
    this.titleRoutes.forEach(element => {
      if (element.route === self.$route.path) {
        self.setTitle(self.cw(self.$t(element.title)))
      }
    }) */
  },
  data () {
    return {
      titleRoutes: [],
      pmeta: {
        type: Object,
        defualt () {
          return {
            pageTitle: 'Test ',
            titleTemplate: title => `${title} - My Website`,
            meta: {
              description: { name: 'description', content: 'Page 1' },
              keywords: { name: 'keywords', content: 'Quasar website' },
              equiv: { 'http-equiv': 'Content-Type', content: 'text/html; charset=UTF-8' },
              ogTitle: {
                name: 'og:title',
                // optional; similar to titleTemplate, but allows templating with other meta properties
                template (ogTitle) {
                  return `${ogTitle} - My Website`
                }
              }
            },
            link: {
              material: { rel: 'stylesheet', href: 'https://fonts.googleapis.com/icon?family=Material+Icons' }
            },
            script: {
              ldJson: {
                type: 'application/ld+json',
                innerHTML: '{ "@context": "http://schema.org" }'
              }
            },
            htmlAttr: { 'xmlns:cc': '', empty: undefined },
            bodyAttr: { 'action-scope': '', empty: undefined },
            noscript: { default: '' }
          }
        }
      }
    }
  },
  meta () {
    return this.pmeta
  }
}
