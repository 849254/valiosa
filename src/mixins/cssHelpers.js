import { defineComponent } from 'vue'
export default defineComponent({
  props: {
  },
  data () {
    return {
      windowHeight: window.innerHeight
    }
  },
  beforeMount () {

  },
  methods: {
    getElementHeight (elementRef) {
      const height = this.$refs[elementRef].clientHeight
      return height
    }
  },
  watch: {

  }
}
