export default {
  props: {
    hasRoutes: {
      type: Array,
      defualt () {
        return ['index']
      }
    },
    hasNotRoutes: {
      type: Array,
      defualt () {
        return ['profile']
      }
    }
  },
  beforeMount () {
    // console.log(`before mount 1 ${this.hasRoutes} : ${this.hasNotRoutes}`)
    // console.log('before mount 2')
    const has = this.hasRoutes ? this.hasRoutes.includes(this.$route.path) : false
    const hasNot = this.hasNotRoutes ? this.hasNotRoutes.includes(this.$route.path) : false
    // console.log('before mount 3')
    if ((has && hasNot) || (!has && !hasNot)) {
      // throw error
    }
    // console.log(`route hasNot: ${hasNot}, has: ${has}`)
    // console.log('before mount 4')
    if (hasNot && !has) {
      console.log('before mount 5')
      this.distruct()
    }
  },
  methods: {
    goToRoute (path) {
      if (this.$route.path !== path) {
        this.$router.push(path)
      }
    },
    validate () {
      if (this.hasRoutes !== undefined && this.hasNotRoutes !== undefined) {
        this.hasRoutes.forEach(element => {
          if (this.hasNotRoutes.includes(element)) {
            // throw error
          }
        })
      }
    },
    distruct () {
      console.log('destroying')
      this.$destroy()
      // this.$el.parentNode.removeChild(this.$el);
    }
  },
  watch: {
    hasRoutes: {
      handler () {
        this.validate()
      },
      deep: true
    },
    hasNotRoutes: {
      handler () {
        this.validate()
      },
      deep: true
    }
  }
}
