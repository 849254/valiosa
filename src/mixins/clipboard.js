import { defineComponent } from 'vue'
export default defineComponent({
    methods: {

    },
    created () {
        this.canCopy = !!navigator.clipboard
    },
    methods: {
        async copy (s) {
            await navigator.clipboard.writeText(s)
        }
    },
    data () {
        return {
            canCopy: false
        }
    },

}
