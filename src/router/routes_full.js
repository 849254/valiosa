/* eslint-disable indent */
const requiresAuth = !process.env.DEV
const isAdmin = !process.env.DEV

const routes = [
    { path: '/login', redirect: { name: 'login' } },
    { path: '/signup', redirect: { name: 'signup' } },
    { path: '/reset', redirect: { name: 'reset' } },
    {
        path: '/admin/',
        alias: '/x',
        meta: {
            requiresAuth: requiresAuth,
            is_admin: isAdmin
        },
        component: () => import('layouts/DashLayout.vue'),
        children: [
            { path: '', component: () => import('pages/admin/DashHome.vue') },
            { path: 'payments', component: () => import('pages/admin/PaymentManagement.vue') },
            { path: 'products', component: () => import('pages/admin/ProductManagement.vue') },
            { path: 'orders', component: () => import('pages/admin/OrderManagement.vue') },
            { path: 'users', component: () => import('pages/admin/UserManagement.vue') },
            { path: 'messages', component: () => import('pages/admin/MessagesManagement.vue') },
            { path: 'content', component: () => import('pages/admin/ContentManagement.vue') },
            { path: 'user_manual', component: () => import('pages/admin/UserManual.vue') }
        ]
    },
    {
        path: '/user',
        component: () => import('layouts/AuthLayout.vue'),
        children: [
            { name: 'login', path: 'login', component: () => import('pages/public/Login.vue') },
            { name: 'signup', path: 'signup', component: () => import('pages/public/Signup.vue') },
            { name: 'reset', path: 'reset', component: () => import('pages/public/Reset.vue') }
        ]
    },
    { path: '/splash', component: () => import('pages/admin/Splash.vue') },
    { path: '/login', redirect: { name: 'login' } },
    { path: '/signup', redirect: { name: 'signup' } },
    { path: '/reset', redirect: { name: 'reset' } },
    { path: '/cart', redirect: { name: 'cart' } },
    { path: '/profile', redirect: { name: 'profile' } },
    { path: '/checkout', redirect: { name: 'checkout' } },
    {
        path: '/user',
        component: () => import('layouts/AuthLayout.vue'),
        children: [
            { name: 'login', path: 'login', component: () => import('pages/public/Login.vue') },
            { name: 'signup', path: 'signup', component: () => import('pages/public/Signup.vue') },
            { name: 'reset', path: 'reset', component: () => import('pages/public/Reset.vue') },
            {
                name: 'profile',
                path: 'profile',
                component: () => import('pages/user/Profile.vue'),
                meta: {
                    requiresAuth: requiresAuth,
                    is_admin: isAdmin
                }
            },
            {
                name: 'cart',
                path: 'cart',
                component: () => import('pages/user/Cart.vue'),
                meta: {
                    requiresAuth: requiresAuth
                }
            },
            {
                name: 'checkout',
                path: 'checkout',
                component: () => import('pages/user/Checkout.vue'),
                meta: {
                    requiresAuth: requiresAuth
                }
            }
        ]
    },
    {
        path: '/',
        component: () => import('layouts/MainLayout.vue'),
        children: [
            { path: '', component: () => import('pages/public/Home.vue') },
            { path: 'product', component: () => import('pages/public/Product.vue') },
            { path: 'categories', component: () => import('pages/public/Categories.vue') },
            { path: 'aboutus', component: () => import('pages/public/Aboutus.vue') },
            { path: 'contactus', component: () => import('pages/public/Contactus.vue') },
            { path: 'privacy', component: () => import('pages/public/Privacy.vue') },
            { path: 'terms', component: () => import('pages/public/Terms.vue') },
            { path: 'sitemap', component: () => import('pages/public/Sitemap.vue') }
        ]
    },

    // Always leave this as last one,
    // but you can also remove it
    {
        path: '/:catchAll(.*)*',
        component: () => import('pages/public/Error.vue')
    }
]

export default routes
