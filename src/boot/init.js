/* eslint-disable indent */
import VueSession from 'vue-session'
import VueMq from 'vue-mq'
import PortalVue from 'portal-vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

export default ({ app, router, Vue }) => {
    Vue.use(VueSession)
    Vue.use(PortalVue)
    Vue.use(VueAxios, axios)

    Vue.prototype.$getPlaceImgUrl = function (w = '1280', h = '720', p = 'nature') {
        return `https://placeimg.com/${w}/${h}/${p}`
    }
    function error (params) {
    }
    // determine is it development or production
    console.log(`process.env.NODE_ENV: ${process.env.NODE_ENV}`)
    let domain
    if (process.env.DEV) {
        domain = 'http://localhost:5001/api'
    } else {
        domain = 'https://www.valiosa.com/api'
    }
    const requestHeaders = {
        'Content-Type': 'application/json'
    }
    let requestUrl
    const mode = 'axios' // axios
    Vue.prototype.$request = function (method, url, query, body, onResponse, onError = error, headers = requestHeaders) {
        requestUrl = `${domain}/${url}`
        console.log(requestUrl)
        requestUrl = new URL(requestUrl.replaceAll('//', '/'))
        // console.log(`url2 ${requestUrl}`)
        requestUrl.search = new URLSearchParams(query)
        // serverAddress = response.headers['server-address']
        // url = queryToUrl(url, query, { headers: headers })
        if (mode === 'fetch') {
            console.log('fetch')
            if (method === 'get') {
                fetch(requestUrl, {
                    method: 'GET',
                    headers: headers
                }).then(response => response.json())
                    .then(data => onResponse(data))
                    .catch((error) => {
                        onError(error)
                    })
            } else if (method === 'post') {
                console.log(`we are sending ${JSON.stringify(body)}`)
                fetch(requestUrl, {
                    method: 'POST',
                    headers: headers,
                    body: JSON.stringify(body)
                }).then(response => response.json())
                    .then(data => onResponse(data))
                    .catch((error) => {
                        onError(error)
                    })
            } else if (method === 'delete') {
                fetch(requestUrl, {
                    method: 'DELETE',
                    headers: headers
                }).then(response => response.json())
                    .then(data => onResponse(data))
                    .catch((error) => {
                        onError(error)
                    })
            } else if (method === 'put') {
                fetch(requestUrl, {
                    method: 'PUT',
                    headers: headers,
                    body: JSON.stringify(body)
                }).then(response => response.json())
                    .then(data => onResponse(data))
                    .catch((error) => {
                        onError(error)
                    })
            } else if (method === 'patch') {
                fetch(requestUrl, {
                    method: 'PATCH',
                    headers: headers,
                    body: JSON.stringify(body)
                }).then(response => response.json())
                    .then(data => onResponse(data))
                    .catch((error) => {
                        onError(error)
                    })
            } else if (method === 'head') {
                fetch(requestUrl, {
                    method: 'HEAD',
                    headers: headers
                }).then(response => response.json())
                    .then(data => onResponse(data))
                    .catch((error) => {
                        onError(error)
                    })
            } else if (method === 'options') {
                fetch(requestUrl, {
                    method: 'OPTIONS',
                    headers: headers
                }).then(response => response.json())
                    .then(data => onResponse(data))
                    .catch((error) => {
                        onError(error)
                    })
            }
        } else if (mode === 'axios') {
            console.log('axios')
            url = requestUrl.toString()
            if (method === 'get') {
                axios.get(url, { params: query, headers: headers }).then((response) => { onResponse(response) }).catch((error) => { onError(error) })
            } else if (method === 'post') {
                axios.post(url, { params: query, headers: headers, data: body }).then((response) => { onResponse(response) }).catch((error) => { onError(error) })
            } else if (method === 'delete') {
                axios.delete(url, { params: query, headers: headers }).then((response) => { onResponse(response) }).catch((error) => { onError(error) })
            } else if (method === 'put') {
                axios.put(url, { params: query, headers: headers, data: body }).then((response) => { onResponse(response) }).catch((error) => { onError(error) })
            } else if (method === 'patch') {
                axios.patch(url, { params: query, headers: headers, data: body }).then((response) => { onResponse(response) }).catch((error) => { onError(error) })
            } else if (method === 'head') {
                axios.head(url, { params: query, headers: headers }).then((response) => { onResponse(response) }).catch((error) => { onError(error) })
            } else if (method === 'options') {
                axios.options(url, { params: query, headers: headers }).then((response) => { onResponse(response) }).catch((error) => { onError(error) })
            }
        }
    }
    Vue.use(VueMq, {
        breakpoints: { // default breakpoints - customize this
            sm: 450,
            md: 1250,
            lg: Infinity
        }
        // defaultBreakpoint: 'sm' // customize this for SSR
    })

    // Vue.prototype.$uploaderPath = `${domain}/`
    Vue.prototype.$env = process.env.DEV ? 'development' : 'production'
    // something to do
    function onFail (e) {
        console.log(`Network Request Faild With Exception ${JSON.stringify(e)}`)
    }
    function paramsToUrl (url, params, paramsAsPayload) {
        let toReturn = ''
        if (paramsAsPayload) {
            toReturn = domain + url
            return toReturn
        } else {
            toReturn = domain + url + '?'
        }
        let amp = ''
        for (const key in params) {
            if (toReturn.endsWith('?')) {
                amp = ''
            } else {
                amp = '&'
            }
            toReturn = `${toReturn}${amp}${key}=${params[key]}`
        }
        if (toReturn.endsWith('?')) {
            toReturn = toReturn.replace('?', '')
        }
        // console.log(`request url is ${toReturn}`)
        return toReturn
    }
    /* Vue.prototype.$resUrl = function getSession () {
                                                    let self = this
                                                    this.$doGet('/users/getSession', {}, function (response) {
                                                      self.session = response.data
                                                    })
                                                  }, */
    Vue.prototype.$doGet = function (url, params, onSuccess, onFailure = onFail) {
        url = paramsToUrl(url, params)
        this.$http.get(url, {}).then((response) => {
            onSuccess(response)
        }).catch((e) => {
            onFailure(e)
        })
    }
    Vue.prototype.$doPost = function (url, params, onSuccess, paramsAsPayload = false, onFailure = onFail) {
        // console.log(`before url : ${url}, params : ${JSON.stringify(params)}, paramsAsPayload : ${paramsAsPayload}`)
        url = paramsToUrl(url, params, paramsAsPayload)
        if (!paramsAsPayload) {
            params = {}
        }
        // console.log(`after url : is ${url},params: ${paramsAsPayload},paramsAsPayload: ${paramsAsPayload}`)
        // axios.post(url, params).then(response => (onSuccess(response)))
        this.$http.post(url, params, { timeout: 0, emulateJSON: true }).then((response) => {
            onSuccess(response)
        }).catch((e) => {
            onFailure(e)
        })
    }
}
