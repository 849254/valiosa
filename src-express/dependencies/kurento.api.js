
// var path = require('path')
// var url = require('url')
// var cookieParser = require('cookie-parser')
// var express = require('express')
// var session = require('express-session')
// var minimist = require('minimist')
// var ws = require('ws')
var kurento = require('kurento-client')

// var fs = require('fs')
// var https = require('https')
const Kurento = {}

/*
var options =
{
  key: fs.readFileSync('keys/server.key'),
  cert: fs.readFileSync('keys/server.crt')
}

var app = express()
 * Management of sessions
 */
// app.use(cookieParser())

/* var sessionHandler = session({
  secret: 'none',
  rolling: true,
  resave: true,
  saveUninitialized: true
}) */

// app.use(sessionHandler)

/*
 * Definition of global variables.
 */

/*
 * Server startup
 *
 *
 * var asUrl = url.parse(argv.as_uri)
var port = asUrl.port
var server = https.createServer(options, app).listen(port, function () {
  console.log('Kurento Tutorial started')
  console.log('Open ' + url.format(asUrl) + ' with a WebRTC capable browser')
})/

var wss = new ws.Server({
  server: server,
  path: '/player'
})

/*
 * Management of WebSocket messages
 *
 *
 *
 * wss.on('connection', function (ws) {

})/

/*
 * Definition of functions
 */

// Recover kurentoClient for the first time.

class MediaPlayer {
  constructor () {
    console.log('Initlizing MediaPlayer')
    this.argv = null
    this.sessions = {}
    this.candidatesQueue = {}
    this.kurentoClient = null
    this.videoUrlToPlay = null
    this.myPlayer = null
    this.myPipeline = null
    this.config = null
  }

  init (config) {
    this.config = config
    console.log(`config is ${JSON.stringify(this.config)}`)
  }

  getKurentoClient (callback) {
    if (this.kurentoClient !== null) {
      return callback(null, this.kurentoClient)
    }
    const self = this
    kurento(this.config.kms_uri, function (error, _kurentoClient) {
      if (error) {
        console.log('Could not find media server at address ' + self.config.kms_uri)
        var prm = `Could not find media server at address ${self.config.kms_uri} . Exiting with error ${error}`
        return callback(prm)
      }
      console.log('server found at ' + self.config.kms_uri)
      self.kurentoClient = _kurentoClient
      callback(null, self.kurentoClient)
    })
  }

  start (sessionId, socket, sdpOffer, callback, videoUrlToPlay) {
    if (!sessionId) {
      // eslint-disable-next-line standard/no-callback-literal
      return callback('Cannot use undefined sessionId')
    }
    const self = this
    this.getKurentoClient(function (error, kurentoClient) {
      if (error) {
        return callback(error)
      }

      kurentoClient.create('MediaPipeline', function (error, pipeline) {
        if (error) {
          return callback(error)
        }
        console.log(`video url is ${videoUrlToPlay}`)
        pipeline.create('PlayerEndpoint',
          { uri: videoUrlToPlay },
          function (error, player) {
            if (error) {
              console.log('error while creating PlayerEndpoint:' + error)
            }
            self.myPlayer = player
            self.myPipeline = pipeline.create('WebRtcEndpoint', function (error, webRtcEndpoint) {
              if (error) {
                console.log('error while creating WebRtcEndpoint' + error)
              }

              if (self.candidatesQueue[sessionId]) {
                while (self.candidatesQueue[sessionId].length) {
                  var candidate = self.candidatesQueue[sessionId].shift()
                  webRtcEndpoint.addIceCandidate(candidate)
                }
              }

              webRtcEndpoint.on('OnIceCandidate', function (event) {
                var candidate = kurento.register.complexTypes.IceCandidate(event.candidate)
                socket.emit('iceCandidate', {
                  candidate: candidate
                })
              })
              player.connect(webRtcEndpoint, function (error, pipeline) {
                if (error) {
                  console.log('connect error: ' + error)
                }

                webRtcEndpoint.processOffer(sdpOffer, function (error, sdpAnswer) {
                  if (error) {
                    self.myPipeline.release()
                    return callback(error)
                  }
                  self.sessions[sessionId] = {
                    pipeline: self.myPipeline,
                    webRtcEndpoint: webRtcEndpoint,
                    player: player
                  }
                  return callback(null, sdpAnswer)
                })
                webRtcEndpoint.gatherCandidates(function (error) {
                  if (error) {
                    self.myPipeline.release()
                    return callback(error)
                  }
                })
                webRtcEndpoint.on('MediaFlowInStateChange', function (state, pad, mediaType) {
                  console.log(`on MediaFlowInStateChange state: ${JSON.stringify(state)}`)
                  player.getVideoInfo(function (error, result) {
                    if (error) {
                      console.log('--error : ' + error)
                    }
                    // FIXME find better event
                    if (state && state.mediaType === 'VIDEO' && state.state === 'FLOWING') {
                      socket.emit('videoInfo', {
                        isSeekable: result.isSeekable,
                        initSeekable: result.seekableInit,
                        endSeekable: result.seekableEnd,
                        videoDuration: result.duration
                      })
                    }
                  })
                })
                player.on('EndOfStream', function () {
                  console.log('on end of stream')
                  socket.emit('playEnd')
                  self.myPipeline.release()
                })

                player.play(function (error) {
                  if (error) {
                    return callback(error)
                  }
                })
              })
            })
          })
      })
    })
  }

  createMediaElements (pipeline, ws, callback) {
    pipeline.create('PlayerEndpoint', function (error, webRtcEndpoint) {
      if (error) {
        return callback(error)
      }

      return callback(null, webRtcEndpoint)
    })
  }

  connectMediaElements (webRtcEndpoint, callback) {
    webRtcEndpoint.connect(webRtcEndpoint, function (error) {
      if (error) {
        return callback(error)
      }
      return callback(null)
    })
  }

  stop (sessionId) {
    if (this.sessions[sessionId]) {
      var pipeline = this.sessions[sessionId].pipeline
      console.info('Releasing pipeline')
      pipeline.release()
      var player = this.sessions[sessionId].player
      console.log('Releasing player')
      player.stop()

      delete this.sessions[sessionId]
      delete this.candidatesQueue[sessionId]
    } else {
      console.error('not known session id - ' + sessionId)
    }
  };

  pause (sessionId) {
    if (this.sessions[sessionId]) {
      this.sessions[sessionId].player.pause()
    } else {
      console.error('not known session id - ' + sessionId)
    }
  }

  resume (sessionId) {
    if (this.sessions[sessionId]) {
      this.sessions[sessionId].player.play()
    } else {
      console.error('not known session id - ' + sessionId)
    }
  }

  seek (sessionId, newPosition) {
    if (this.sessions[sessionId]) {
      this.sessions[sessionId].player.setPosition(newPosition)
    } else {
      console.error('not known session id - ' + sessionId)
    }
  }

  onIceCandidate (sessionId, _candidate) {
    var candidate = kurento.register.complexTypes.IceCandidate(_candidate)
    if (this.sessions[sessionId]) {
      console.info('Sending candidate')
      var webRtcEndpoint = this.sessions[sessionId].webRtcEndpoint
      webRtcEndpoint.addIceCandidate(candidate)
    } else {
      console.info('Queueing candidate')
      if (!this.candidatesQueue[sessionId]) {
        this.candidatesQueue[sessionId] = []
      }
      this.candidatesQueue[sessionId].push(candidate)
    }
  }

  getPosition (sessionId) {
    if (this.sessions[sessionId]) {
      return this.sessions[sessionId].player.getPosition(function (error, result) {
        if (error) {
          return
        }
        return result
      })
    } else {
      console.error('not known session id - ' + sessionId)
    }
  }
}

Kurento.MediaPlayer = new MediaPlayer()

module.exports = Kurento
