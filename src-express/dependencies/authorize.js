const jwt = require('jsonwebtoken')

var accessTokenSecret = ''
var refreshTokenSecret = ''
const refreshTokens = []

exports.setTokensSecrets = (ats, rts) => {
  accessTokenSecret = ats
  refreshTokenSecret = rts
}

exports.generateTokens = (user) => {
  const accessToken = jwt.sign({ username: user.username, role: user.role }, accessTokenSecret, { expiresIn: '20m' })
  const refreshToken = jwt.sign({ username: user.username, role: user.role }, refreshTokenSecret)
  return { accessToken, refreshToken }
}

exports.refreshToken = (req, res, next) => {
  const { token } = req.body

  if (!token) {
    return res.sendStatus(401)
  }

  if (!refreshTokens.includes(token)) {
    return res.sendStatus(403)
  }

  jwt.verify(token, refreshTokenSecret, (err, user) => {
    if (err) {
      return res.sendStatus(403)
    }

    const accessToken = jwt.sign({ username: user.username, role: user.role }, accessTokenSecret, { expiresIn: '20m' })

    res.json({
      accessToken
    })
  })
}
exports.authJWT = (req, res, next) => {
  const authHeader = req.headers.authorization
  if (authHeader) {
    const token = authHeader.split(' ')[1]
    jwt.verify(token, accessTokenSecret, (err, user) => {
      if (err) {
        return res.sendStatus(403)
      }
      req.user = user
      next()
    })
  } else {
    res.sendStatus(401)
  }
}

exports.destroy = (req, res, next) => {
  const { token } = req.body
  refreshTokens = refreshTokens.filter(token => t !== token)
}
