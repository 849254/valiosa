var env = process.env.NODE_ENV || 'dev'
const tenpay = require('tenpay')

module.exports = class db {
  constructor () {
    this.config = {}
    this.api = {}
    this.sandBoxApi = {}
  }

  async init () {
    if (env === 'dev') {
      this.config = {
        appid: 'wx7361c0cbf0157f36',
        mchid: '1488570482',
        partnerKey: '微信支付安全密钥',
        pfx: require('fs').readFileSync('证书文件路径'),
        notify_url: '支付回调网址',
        spbill_create_ip: 'IP地址'
      }
      // 调试模式(传入第二个参数为true, 可在控制台输出数据)
      // eslint-disable-next-line new-cap
      this.api = new tenpay(this.config, true)
      // 沙盒模式(用于微信支付验收)
      this.sandBoxApi = await tenpay.sandbox(this.config)
    } else {
      this.config = {
        appid: 'wx7361c0cbf0157f36',
        mchid: '1488570482',
        partnerKey: '微信支付安全密钥',
        pfx: require('fs').readFileSync('证书文件路径'),
        notify_url: '支付回调网址',
        spbill_create_ip: 'IP地址'
      }
      // 方式一
      // const api = new tenpay(config);
      // 方式二
      this.api = tenpay.init(this.config)
    }
    app.use(bodyParser.text({
      type: '*/xml'
    }))

    // 支付结果通知/退款结果通知
    router.post('/xxx', api.middlewareForExpress('pay'), (req, res) => {
      const info = req.weixin

      // 业务逻辑...

      // 回复消息(参数为空回复成功, 传值则为错误消息)
      res.reply('错误消息' || '')
    })

    // 扫码支付模式一回调
    router.post('/xxx', api.middlewareForExpress('nativePay'), (req, res) => {
      const info = req.weixin

      // 业务逻辑和统一下单获取prepay_id...

      // 响应成功或失败(第二个可选参数为输出错误信息)
      res.replyNative(prepay_id, err_msg)
    })
  }
}
