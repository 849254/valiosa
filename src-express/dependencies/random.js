const Random = {}
class Strings {
  constructor () {
    console.log('')
  }

  wxNonce32 () {
    var result = ''
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    var charactersLength = characters.length
    for (var i = 0; i < 32; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength))
    }
    console.log(`result is ${result}`)
    return result
  }
}

Random.Strings = new Strings()

module.exports = Random
