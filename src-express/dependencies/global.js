var env = process.platform === 'linux' || 'dev',
  composedPublicPath,
  composedUploadPath,
  composedSlidePath,
  composedClassVideosPath

if (env === 'dev') {
  console.log('Environment is Development')
  composedPublicPath = '/Users/theanomaly/Work/JiuHuan/ibiplus/qibi/back-end/public'
} else {
  console.log('Environment is Production')
  composedPublicPath = '/home/deploy/apps/express/ibiplus.js/back-end/public'
}
// console.log(`Global path is ${composedPublicPath}`)

composedUploadPath = composedPublicPath + '/uploads'
composedSlidePath = composedPublicPath + '/classes/'
composedClassVideosPath = composedPublicPath + '/classes/'
module.exports = {
  publicPath: composedPublicPath,
  uploadPath: composedUploadPath,
  slidesPath: composedSlidePath,
  classVideosPath: composedClassVideosPath
}
