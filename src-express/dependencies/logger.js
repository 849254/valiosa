var loggerConfig = {}
exports.setConfig = function (config) {
  loggerConfig = config
}

exports.log = function (obj, msg) {
  console.log(`Logger: ${obj}: ${msg}`)
}
