const mongoose = require('mongoose')
const path = require('path')
const ObjectID = require('mongodb').ObjectID
const User = require('./models/user.model')
// const Role = require('./models/role.model')
const Permission = require('./models/permission.model')
const Product = require('./models/product.model')
const ProductCategory = require('./models/productCategory.model')
const ProductComment = require('./models/productComment.model')
const Order = require('./models/order.model')
const Notification = require('./models/notification.model')
const Payment = require('./models/payment.model')
const PaymentMethod = require('./models/paymentMethod.model')
const Cart = require('./models/cart.model')
const Store = require('./models/store.model')

const env = process.env.NODE_ENV || 'dev'

const fse = require('fs-extra')

module.exports = class db {
  constructor () {
    this.connectionString = 'mongodb+srv://{DBUSERNAME}:{DBPASSWORD}@{DBHOST}/{DBNAME}?retryWrites=true&w=majority'

    if (env == 'dev') {
      this.seed = false
      this.connectionString = `mongodb://localhost:27017/valiosa`
    } else {
      this.connectionString.replace('{DBUSERNAME}', 'this.config.backend.service.env.DBUsername')
      this.connectionString.replace('{DBPASSWORD}', 'this.config.backend.service.env.DBPassword')
      this.connectionString.replace('{DBHOST}', 'this.config.backend.service.env.DBHost')
      this.connectionString.replace('{DBNAME}', 'this.config.backend.service.env.DBName')
    }

    this.db = null

  }

  getID (template, index) {
    return this.templateIds[template][index]
  }

  generateID (template, index) {
    var id = ObjectID(this.ids[index])
    this.templateIds[template].push(id)
    return id
  }

  async init () {
    console.log('Initalizing Database')
    console.log(this.connectionString)
    mongoose.connect(this.connectionString, {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true
    })
    this.db = mongoose.connection
    this.db.on('error', console.error.bind(console, 'connection error:'))
    const self = this
    this.db.once('open', async function () {
      console.log('Connection with database succeeded.')
      if (self.seed) {
        console.log('Perceeding with seed')
        const seedData = fse.readJSONSync(`${__dirname}/seed.json`)
        /* template definitions */
        const userSeeds = seedData.User
        // const roleSeeds = seedData.Role 
        const permissionSeeds = seedData.Permission
        const productSeeds = seedData.Product
        const productCategorySeeds = seedData.ProductCategory
        const orderSeeds = seedData.Order
        const notificationSeeds = seedData.Notification
        const paymentSeeds = seedData.Payment
        const paymentMethodSeeds = seedData.PaymentMethod
        const cartSeeds = seedData.Cart
        const storeSeeds = seedData.Store
        const productCommentSeeds = seedData.ProductComment

        console.log('Cleaning Database')
        await this.db.listCollections().forEach(function (collection) {
          console.log('Dropping Collection : ' + collection.name)
          self.db.collection(collection.name).drop()
        })

        console.log('Cleaning Directories')
        // fs.emptyDirSync(globals.uploadPath)
        console.log('Seeding Database\n')

        /* template seeddings */
        userSeeds.forEach(function (user) {
          var newUser = new User(user)
          newUser.save()
        })

        /*roleSeeds.forEach(function (role) {
          var newRole = new Role(role)
          newRole.save()
        })*/

        permissionSeeds.forEach(function (permission) {
          var newPermission = new Permission(permission)
          newPermission.save()
        })

        productSeeds.forEach(function (product) {
          var newProduct = new Product(product)
          newProduct.save()
        })

        productCommentSeeds.forEach(function (productComment) {
          var newProductComment = new ProductComment(productComment)
          newProductComment.save()
        })

        productCategorySeeds.forEach(function (productCategory) {
          var newProductCategory = new ProductCategory(productCategory)
          newProductCategory.save()
        })

        orderSeeds.forEach(function (order) {
          var newOrder = new Order(order)
          newOrder.save()
        })

        notificationSeeds.forEach(function (notification) {
          var newNotification = new Notification(notification)
          newNotification.save()
        })

        paymentSeeds.forEach(function (payment) {
          var newPayment = new Payment(payment)
          newPayment.save()
        })

        paymentMethodSeeds.forEach(function (paymentMethod) {
          var newPaymentMethod = new PaymentMethod(paymentMethod)
          newPaymentMethod.save()
        })

        cartSeeds.forEach(function (cart) {
          var newCart = new Cart(cart)
          newCart.save()
        })

        storeSeeds.forEach(function (store) {
          var newStore = new Store(store)
          newStore.save()
        })
      }
    })
  }
}
