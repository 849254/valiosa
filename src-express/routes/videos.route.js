const express = require('express'),
  router = express.Router(),
  video = require('../controllers/videos.controller')
  const authorize = require('../dependencies/authorize'),
  { validate } = require('express-validation')

  router.put('/updatePost',authorize.authJWT,validate(video.updatePostValidation, {}, {}), video.updatePost)

module.exports = router
