// var path = require("path");
const express = require('express')
const router = express.Router()
const authorize = require('../dependencies/authorize')
const dashboardController = require('../controllers/dashboards.controller'),
{ validate } = require('express-validation')

// home
router.get('/doGetUserStats', validate( dashboardController.doGetUserStatsValidation, {}, {}), authorize.authJWT,dashboardController.doGetUserStats)
router.get('/doGetFinacialStats', validate( dashboardController.doGetFinacialStatsValidation, {}, {}),authorize.authJWT, dashboardController.doGetFinacialStats)
router.get('/doGetCourseStats',  validate( dashboardController.doGetCoursesStatValidation, {}, {}),authorize.authJWT,dashboardController.doGetCoursesStat)
// email
router.post('/doGetMyEmails', validate( dashboardController.doGetMyEmailsValidation, {}, {}), authorize.authJWT,dashboardController.doGetMyEmails)
router.post('/sendEmail',  validate( dashboardController.sendEmailValidation, {}, {}),authorize.authJWT,dashboardController.sendEmail)
router.post('/composeEmail', validate( dashboardController.composeEmailValidation, {}, {}),authorize.authJWT,dashboardController.composeEmail)
router.post('/deleteEmail',validate( dashboardController.deleteEmailValidation, {}, {}), authorize.authJWT,dashboardController.deleteEmail)
// users
router.get('/doGetUsers',validate( dashboardController.doGetUsersValidation, {}, {}), authorize.authJWT,dashboardController.doGetUsers)
router.post('/doDeleteUser',validate( dashboardController.doDeleteUserValidation, {}, {}),authorize.authJWT, dashboardController.doDeleteUser)
router.post('/doBanUser', validate( dashboardController.doBanUserValidation, {}, {}),authorize.authJWT, dashboardController.doBanUser)
// tutors
router.get('/doGetTutors',  validate( dashboardController.doGetTutorsValidation, {}, {}),authorize.authJWT,dashboardController.doGetTutors)
router.get('/doGetTutorQualifications',  validate( dashboardController.doGetTutorQualificationsValidation, {}, {}),authorize.authJWT,dashboardController.doGetTutorQualifications)
router.post('/toggolTutorQualification', validate( dashboardController.toggolTutorQualificationValidation, {}, {}), authorize.authJWT,dashboardController.toggolTutorQualification)
// router.post('/doApproveTutor', dashboardController.doApproveTutor)
// router.post('/doRevokeTutor', dashboardController.doRevokeTutor)
// courses
router.get('/doGetCourses', validate( dashboardController.doGetCoursesValidation, {}, {}), authorize.authJWT,dashboardController.doGetCourses)
router.post('/toggoleCourseAproval',  validate( dashboardController.toggoleCourseAprovalValidation, {}, {}),authorize.authJWT,dashboardController.toggoleCourseAproval)
// finacial management
router.get('/doGetPaymentRecords', validate( dashboardController.doGetPaymentRecordsValidation, {}, {}),authorize.authJWT, dashboardController.doGetPaymentRecords)
router.get('/doGetPaymentsStats', validate( dashboardController.doGetPaymentsStatsValidation, {}, {}),authorize.authJWT, dashboardController.doGetPaymentsStats)
// pages
router.get('/doGetPageContent', validate( dashboardController.doGetPageContentValidation, {}, {}),authorize.authJWT, dashboardController.doGetPageContent)
router.post('/doUpdatePageContent', validate( dashboardController.doUpdatePageContentValidation, {}, {}),authorize.authJWT, dashboardController.doUpdatePageContent)
// services
// // sms
router.get('/doGetSmsRecords', validate( dashboardController.doGetSmsRecordsValidation, {}, {}), authorize.authJWT,dashboardController.doGetSmsRecords)
router.get('/doGetSmsStats',  validate( dashboardController.doGetSmsStatsValidation, {}, {}),authorize.authJWT,dashboardController.doGetSmsStats)
// // wechat
router.get('/doGetWechatSettings', validate( dashboardController.doGetWechatSettingsValidation, {}, {}), authorize.authJWT,dashboardController.doGetWechatSettings)
router.post('/doSaveWechatSettings', validate( dashboardController.doSaveWechatSettingsValidation, {}, {}),authorize.authJWT, dashboardController.doSaveWechatSettings)
router.get('/doGetWechatMenu',  validate( dashboardController.doGetWechatMenuValidation, {}, {}),authorize.authJWT,dashboardController.doGetWechatMenu)
router.post('/doSaveWechatMenu', validate( dashboardController.doSaveWechatMenuValidation, {}, {}), authorize.authJWT,dashboardController.doSaveWechatMenu)

module.exports = router
