const express = require('express'),
  router = express.Router(),
  uploadController = require('../controllers/uploads.controller')
  const authorize = require('../dependencies/authorize'),
  { validate } = require('express-validation')


router.post('/uploadFile',authorize.authJWT,validate(uploadController.uploadFileValidation, {}, {}), uploadController.uploadFile)
router.get('/getUploadById',authorize.authJWT,validate(uploadController.getUploadByIdValidation, {}, {}), uploadController.getUploadById)
module.exports = router
