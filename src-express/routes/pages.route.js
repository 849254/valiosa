// var path = require('path')
const express = require('express')
const router = express.Router()
const pagesController = require('../controllers/pages.controller')
router.get('/test', pagesController.test)
module.exports = router
/*
module.exports = function (app, db) {
    // var Project = db.Project;
    // pages
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    // credentials
    app.get('/getSession', async function (req, res) {
        res.json(req.session)
    })
    app.post('/doLogout', async function (req, res) {
        req.session.destroy()
        res.json({ success: true })
    })
    app.post('/doLogin', async function (req, res) {
        console.log("loging in...");
        var success = false;
        var msg = "username or password is wrong"
        var c = await db.getCollection("users");
        var user = await c.findOne({ username: req.body.username });
        console.log(`found ${JSON.stringify(user)}`);
        if (user != null) {
            // success = user.authenticate(req.body.username)
            success = user.password == req.body.password
        }
        if (success) {
            req.session.userId = user.id
            req.session.isAdmin = user.is_admin
            msg = "success"
        }
        res.json({ success: success, msg: msg })
    });

    app.post('/doSignup', async function (req, res) {
        console.log("Signing up...");
        var success = false;
        var msg = "username is already taken"
        var c = await db.getCollection("users");
        var user = await c.findOne({ username: req.body.username });
        console.log(`found ${JSON.stringify(user)}`);
        if (user == null) {
            msg = "Sign up Faild"
            user = await c.insertOne({ username: req.body.username, password: req.body.password });
            success = true
            msg = "success"
        }
        res.json({ success: success, msg: msg })
    });

    app.post('/reset', async function (req, res) {
        console.log("Resetting...");
        var success = false;
        var msg = "username is already taken"
        var c = await db.getCollection("users");
        var user = await c.findOne({ username: req.body.username });
        if (user == null) {
            msg = "Sign up Faild"
            user = new User({ size: 'small' });
            success = user.saveSync();
        }
        if (success) {
            msg = "success"
        }
        res.json({ success: success, msg: msg })
    });
    // goals
    app.get('/getGoals', async function (req, res) {

    });

    app.post('/addGoal', async function (req, res) {

    });

    app.post('/updateGoal', async function (req, res) {

    });

    app.post('/deleteGoal', async function (req, res) {

    });

    // Stories
    app.get('/getStories', async function (req, res) {

    });

    app.post('/addStory', async function (req, res) {

    });

    app.post('/updateStory', async function (req, res) {

    });

    app.post('/removeStory', async function (req, res) {

    });
    // Update
    app.get('/getUpdates', async function (req, res) {

    });

    app.get('/addUpdate', async function (req, res) {

    });

    app.get('/removeUpdate', async function (req, res) {

    });
}
*/
