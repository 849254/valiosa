const express = require('express'),
    router = express.Router(),
    blogController = require('../controllers/blogs.controller'),
    authorize = require('../dependencies/authorize'),
    { validate } = require('express-validation')

router.get('/getPosts',authorize.authJWT,validate(blogController.getPostsValidation, {}, {}), blogController.getPosts)
router.get('/getPost', authorize.authJWT,validate(blogController.getPostValidation, {}, {}),blogController.getPost)
router.post('/addPost', authorize.authJWT,validate(blogController.addPostValidation, {}, {}),blogController.addPost)
router.put('/updatePost',authorize.authJWT,validate(blogController.updatePostValidation, {}, {}), blogController.updatePost)
router.delete('/deletePost',authorize.authJWT,validate(blogController.deletePostValidation, {}, {}), blogController.deletePost)
router.get('/getCategs',authorize.authJWT,validate(blogController.getCategsValidation, {}, {}), blogController.getCategs)
router.post('/addCateg',authorize.authJWT,validate(blogController.addCategValidation, {}, {}), blogController.addCateg)
router.put('/updateCateg',authorize.authJWT,validate(blogController.updateCategValidation, {}, {}), blogController.updateCateg)
router.delete('/deleteCateg',authorize.authJWT,validate(blogController.deleteCategValidation, {}, {}), blogController.deleteCateg)
router.get('/getComments', authorize.authJWT,validate(blogController.getCommentsValidation, {}, {}),blogController.getComments)
router.post('/addComment',authorize.authJWT,validate(blogController.addCommentValidation, {}, {}), blogController.addComment)
router.put('/updateComment',authorize.authJWT,validate(blogController.updateCommentValidation, {}, {}), blogController.updateComment)
router.delete('/deleteComment', authorize.authJWT,validate(blogController.deleteCommentValidation, {}, {}),blogController.deleteComment)
module.exports = router
