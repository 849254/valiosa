const payment = require('../models/payment.model'),
    { Joi } = require('express-validation')

exports.getAllPaymentsValidation = {
        headers: Joi.object(),
        params: Joi.object(),
        query: Joi.object(),
        cookies: Joi.object(),
        signedCookies: Joi.object(),
        body: Joi.object(),
    }
exports.getAllPayments = async function (req, res) {
        var payments = {}
        res.json({ success: true, payments: payments })
    }
exports.getPaymentValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.getPayment = async function (req, res) {
    var payment = {}
    res.json({ success: true, payment: payment })
}

exports.addPaymentValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.addPayment = async function (req, res) {
    var payment = {}
    res.json({ success: true })
}

exports.updatePaymentValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.updatePayment = async function (req, res) {
    var payment = {}
    res.json({ success: true })
}

exports.deletePaymentValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.deletePayment = async function (req, res) {
    var payment = {}
    res.json({ success: true })
}