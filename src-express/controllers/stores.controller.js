const store = require('../models/store.model'),
    { Joi } = require('express-validation')

exports.getAllStoresValidation = {
        headers: Joi.object(),
        params: Joi.object(),
        query: Joi.object(),
        cookies: Joi.object(),
        signedCookies: Joi.object(),
        body: Joi.object(),
    }
exports.getAllStores = async function (req, res) {
        var stores = {}
        res.json({ success: true, stores: stores })
    }
exports.getStoreValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.getStore = async function (req, res) {
    var store = {}
    res.json({ success: true, stores: stores })
}

exports.addStoreValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.addStore = async function (req, res) {
    var store = {}
    res.json({ success: true })
}

exports.updateStoreValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.updateStore = async function (req, res) {
    var store = {}
    res.json({ success: true })
}

exports.deleteStoreValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.deleteStore = async function (req, res) {
    var store = {}
    res.json({ success: true })
}