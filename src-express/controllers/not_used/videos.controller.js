/* eslint-disable no-unused-vars */
var result = {}
const post = require('../models/post.model')
const postCategory = require('../models/postCategory.model')
const comment = require('../models/comment.model'),
    { Joi } = require('express-validation')

exports.updatePostValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}

exports.updatePost = async function (req, res) {

    // operation
    // reponse
    const path = 'public/assets/nature.mp4'

    fs.stat(path, (err, stat) => {

        // Handle file not found
        if (err !== null && err.code === 'ENOENT') {
            res.sendStatus(404)
        }

        const fileSize = stat.size
        const range = req.headers.range

        if (range) {

            const parts = range.replace(/bytes=/, "").split("-")

            const start = parseInt(parts[0], 10)
            const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1

            const chunksize = (end - start) + 1
            const file = fs.createReadStream(path, { start, end })
            const head = {
                'Content-Range': `bytes ${start}-${end}/${fileSize}`,
                'Accept-Ranges': 'bytes',
                'Content-Length': chunksize,
                'Content-Type': 'video/mp4',
            }

            res.writeHead(206, head)
            file.pipe(res)
        } else {
            const head = {
                'Content-Length': fileSize,
                'Content-Type': 'video/mp4',
            }

            res.writeHead(200, head)
            fs.createReadStream(path).pipe(res)
        }
    })
    res.json({ success: true })
}
