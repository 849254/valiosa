const Content = require('../models/content.model'),
{ Joi } = require('express-validation')

exports.saveTermsOfServiceValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.saveTermsOfService = async function (req, res) {
  await Content.updateTermsOfService(req.query.termsOfService)
  res.json({ success: true })
}
exports.updateAboutIntroValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.updateAboutIntro = async function (req, res) {
  await Content.updateAboutIntro(req.query)
  res.json({ success: true })
}
exports.savePrivacyPolicyValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.savePrivacyPolicy = async function (req, res) {
  await Content.updatePrivacyPolicy(req.query.privacyPolicy)
  res.json({ success: true })
}
exports.getAllContentsValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.getAllContents = async function (req, res) {
  var contents = await Content.find()
  res.json({ success: true, contents: contents })
}
exports.getSpecificContentValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.getSpecificContent = async function (req, res) {
  console.log(`${req.query.page}`)
  var contents = await Content.find({ page: req.query.page })
  res.json({ success: true, contents: contents })
}
exports.addTeamMemberImgValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.addTeamMemberImg = async function (req, res) {
  res.json({ success: true })
}
exports.saveAllContentsValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.saveAllContents = async function (req, res) {
  var contents = await req.query.contents
  await Content.updateMany(contents)
  res.json({ success: true })
}
exports.updateMileStonesValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.updateMileStones = async function (req, res) {
  var mileSones = req.body.mileSones
  await Content.updateMileStones(mileSones)
  res.json({ success: true })
}
exports.updateTeamMembersValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.updateTeamMembers = async function (req, res) {
  var teamMembers = req.body.teamMembers
  console.log(`${JSON.stringify(req.body.teamMembers)}`)
  await Content.updateTeamMembers(teamMembers)
  res.json({ success: true })
}
exports.getArticlesValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.getArticles = async function (req, res) {
  var result = await Article.getArticles(req.query)
  res.json(result)
}
exports.getOtherArticlesValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.getOtherArticles = async function (req, res) {
  var result = await Article.getArticles(req.query)
  res.json(result)
}
exports.getArticleValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.getArticle = async function (req, res) {
  var article = await Article.findOne({ _id: req.query.id })
  res.json({ success: true, article: article })
}
exports.getArticleCategoriesValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.getArticleCategories = async function (req, res) {
  var articleCategories = await ArticleCategory.find()
  res.json({ success: true, articleCategories: articleCategories })
}
exports.addArticleCategoryValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.addArticleCategory = async function (req, res) {
  var articleCategory = await ArticleCategory.create(req.query)
  await articleCategory.save()
  res.json({ success: true })
}
exports.updateArticleCategoryValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.updateArticleCategory = async function (req, res) {
  await ArticleCategory.updateOne({
    _id: req.query._id,
    name: req.query.name
  })
  res.json({ success: true })
}
exports.deleteArticleCategoryValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.deleteArticleCategory = async function (req, res) {
  await ArticleCategory.deleteOne({ _id: req.query._id })
  res.json({ success: true })
}
exports.addArticleValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}

exports.addArticle = async function (req, res) {
  var result = Article.addArticle(req.query)
  res.json(result)
}
exports.updateArticleValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.updateArticle = async function (req, res) {
  var article = await req.query.article
  await Article.updateOne(article)
  res.json({ success: true })
}
exports.deleteArticleValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.deleteArticle = async function (req, res) {
  await Article.deleteOne({ _id: req.query.id })
  res.json({ success: true })
}
exports.doLogValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doLog = async function (req, res) {
  var log = req.query
  log.ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
  log.datetime = new Date(Date.now())
  await Content.doLog(log)
  res.json({ success: true })
}
exports.doGetLogsValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doGetLogs = async function (req, res) {
  console.log('doGetLogs loging ...')
  console.log(`request with params ${JSON.stringify(req.query)}`)
  var logs = await Content.doGetLogs(req.query)
  res.json({ success: true, logs: logs })
}
exports.saveSlidesValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.saveSlides = async function (req, res) {
  console.log(`recived query ${JSON.stringify(req.query)}`)
  console.log(`recived body ${JSON.stringify(req.body)}`)
  await Content.saveSlides(req.body)
  res.json({ success: true })
}
exports.getPersonalHomeContentValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.getPersonalHomeContent = async function (req, res) {
  console.log(`getPersonalHomeContent query is : ${JSON.stringify(req.query)}`)
  var stats = {}
  if (req.query.role_id == 1) {
    // get users countries
    // get each user role count
    // get approved tutors and waiting for aproval and thir total income
    // get courses.count and classses.count and total enrolled students
    // get financial things
    stats = {
      users_count: { admin_count: 54, tutor_count: 234, student_count: 3254 },
      tutor_stats: {
        qualified_tutors_count: 543,
        pending_qualification_count: 32432,
        toturs_total_income: 123
      },
      courses_stats: {
        total_courses_count: 3254,
        total_classes_count: 678,
        total_enrolled_student_count: 567
      },
      financial_stats: {
        total_income: 212,
        driven_income: 324,
        averge_user_driven_income: 3232
      }
    }
  } else if (req.query.role_id == 2) {
    stats = {
      tutor_stats: {
        myenrolled_students: 1,
        my_total_courses: 1,
        my_total_classes: 1,
        my_total_income: 1
      }
    }
  } else if (req.query.role_id == 3) {
    stats = {
      tutor_stats: {
        myenrolled_students: 1,
        my_total_courses: 1,
        my_total_classes: 1,
        my_total_income: 1
      }
    }
  }
  res.json({ success: true, stats: stats })
}
