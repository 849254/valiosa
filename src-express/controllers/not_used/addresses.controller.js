const address = require('../models/address.model'),
    { Joi } = require('express-validation')

exports.getAllAddressesValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.getAllAddresses = async function (req, res) {
    var addresses = {}
    res.json({ success: true, addresses: addresses })
}

exports.getAddressValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.getAddress = async function (req, res) {
    var addresses = {}
    res.json({ success: true, addresses: addresses })
}

exports.addAddressValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.addAddress = async function (req, res) {
    var addresses = {}
    res.json({ success: true })
}

exports.updateAddressValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.updateAddress = async function (req, res) {
    var addresses = {}
    res.json({ success: true })
}

exports.deleteAddressValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.deleteAddress = async function (req, res) {
    var addresses = {}
    res.json({ success: true })
}