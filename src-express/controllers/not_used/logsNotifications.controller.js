/* eslint-disable no-unused-vars */
var result = {}
const log = require('../models/log.model')
const notification = require('../models/notification.model'),
  { Joi } = require('express-validation')

exports.loginValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}

exports.login = async function (req, res) {
  console.log('extensions want status')
  res.json({})
}
