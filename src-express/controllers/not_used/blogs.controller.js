/* eslint-disable no-unused-vars */
var result = {}
const post = require('../models/post.model')
const postCategory = require('../models/postCategory.model')
const comment = require('../models/comment.model'),
    { Joi } = require('express-validation')

exports.getPostsValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.getPosts = async function (req, res) {

    // operation
    // reponse
    var posts = [
    ]
    res.json({ success: true, posts: posts })
}
exports.getPostValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.getPost = async function (req, res) {

    // operation
    // reponse
    var post = { id: '13fhkjrerejhgrejhgwejhjgrk', img: 'https://placeimg.com/1280/720/nature', title: 'server article 1', abs: '', url: '', date: '2020/01/01', author: { id: '', name: "TP", avatar: '' } }
    res.json({ success: true, post: post })
}
exports.addPostValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}

exports.addPost = async function (req, res) {

    // operation
    // reponse
    res.json({ success: true })
}
exports.updatePostValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.updatePost = async function (req, res) {

    // operation
    // reponse
    res.json({ success: true })
}
exports.deletePostValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.deletePost = async function (req, res) {

    // operation
    // reponse
    res.json({ success: true })
}
exports.getCategsValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.getCategs = async function (req, res) {

    // operation
    // reponse
    res.json({ success: true })
}
exports.addCategValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}

exports.addCateg = async function (req, res) {

    // operation
    // reponse
    res.json({ success: true })
}
exports.updateCategValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.updateCateg = async function (req, res) {

    // operation
    // reponse
    res.json({ success: true })
}
exports.deleteCategValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.deleteCateg = async function (req, res) {

    // operation
    // reponse
    res.json({ success: true })
}
exports.getCommentsValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.getComments = async function (req, res) {

    // operation
    // reponse
    res.json({ success: true })
}
exports.addCommentValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.addComment = async function (req, res) {

    // operation
    // reponse
    res.json({ success: true })
}
exports.updateCommentValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.updateComment = async function (req, res) {

    // operation
    // reponse
    res.json({ success: true })
}
exports.deleteCommentValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.deleteComment = async function (req, res) {

    // operation
    // reponse
    res.json({ success: true })
}