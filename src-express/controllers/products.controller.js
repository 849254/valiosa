const product = require('../models/product.model'),
    { Joi } = require('express-validation')
// get Categories 
// add Category 
// delete Category
// update Category
// 
exports.getAllProductsValidation = {
    headers: Joi.object(),
    params: Joi.object({
        storeId: Joi.string()
            .required()
    }),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.getAllProducts = async function (req, res) {
    var storeID = req.params.storeId
    var products = {}
    res.json({ success: true, products: products })
}

exports.getProductValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.getProduct = async function (req, res) {
    var products = {}
    res.json({ success: true, products: products })
}

exports.addProductValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.addProduct = async function (req, res) {
    var products = {}
    res.json({ success: true })
}

exports.updateProductValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.updateProduct = async function (req, res) {
    var products = {}
    res.json({ success: true })
}

exports.deleteProductValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.deleteProduct = async function (req, res) {
    var products = {}
    res.json({ success: true })
}