const productCategory = require('../models/productCategory.model'),
    { Joi } = require('express-validation')

exports.getAllProductCategoriesValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.getAllProductCategories = async function (req, res) {
    var productCategorys = {}
    res.json({ success: true, productCategorys: productCategorys })
}

exports.getProductCategoryValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.getProductCategory = async function (req, res) {
    var productCategorys = {}
    res.json({ success: true, productCategorys: productCategorys })
}

exports.addProductCategoryValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.addProductCategory = async function (req, res) {
    var productCategorys = {}
    res.json({ success: true })
}

exports.updateProductCategoryValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.updateProductCategory = async function (req, res) {
    var productCategorys = {}
    res.json({ success: true })
}

exports.deleteProductCategoryValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.deleteProductCategory = async function (req, res) {
    var productCategorys = {}
    res.json({ success: true })
}