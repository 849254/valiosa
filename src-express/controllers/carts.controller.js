const cart = require('../models/cart.model'),
    { Joi } = require('express-validation')


exports.getAllCartsValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.getAllCarts = async function (req, res) {
    var cart = {}
    res.json({ success: true, cart: cart })
}
exports.getCartValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.getCart = async function (req, res) {
    var cart = {}
    res.json({ success: true, cart: cart })
}

exports.addCartValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.addCart = async function (req, res) {
    var cart = {}
    res.json({ success: true })
}

exports.updateCartValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}

exports.updateCart = async function (req, res) {
    var cart = {}
    res.json({ success: true })
}

exports.deleteCartValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.deleteCart = async function (req, res) {
    var cart = {}
    res.json({ success: true })
}