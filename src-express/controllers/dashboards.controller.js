const User = require('../models/user.model')
const Notification = require('../models/notification.model'),
  { Joi } = require('express-validation')


exports.doGetUserStatsValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doGetUserStats = function (req, res) {
  res.json({})
}

exports.doGetFinacialStatsValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doGetFinacialStats = function (req, res) {
  res.json({})
}

exports.doGetCoursesStatValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doGetCoursesStat = function (req, res) {
  res.json({})
}

exports.deleteEmailValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.deleteEmail = function (req, res) {
  res.json({})
}

exports.doGetMyEmailsValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doGetMyEmails = function (req, res) {
  res.json({})
}

exports.sendEmailValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.sendEmail = function (req, res) {
  res.json({})
}

exports.composeEmailValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.composeEmail = function (req, res) {
  res.json({})
}

exports.doGetUsersValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doGetUsers = function (req, res) {
  User.find({}, function (err, result) {
    if (err) throw err
    console.log()
    res.json(result)
  })
}

exports.doDeleteUserValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doDeleteUser = function (req, res) {
  res.json({})
}

exports.doBanUserValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doBanUser = function (req, res) {
  res.json({})
}

exports.doGetTutorsValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doGetTutors = async function (req, res) {
  var tutors = await User.find(req.query)
  for (var i = 0; i < tutors.length; i++) {
    tutors[i].courses = await Course.find({ tutor_id: tutors[i].id })
    tutors[i].course_count = tutors[i].courses.length
    for (var x = 0; x < tutors[i].courses.length; x++) {
      tutors[i].driven_income = tutors[i].driven_income + tutors[i].courses[x].driven_income
      tutors[i].personal_income = tutors[i].personal_income + tutors[i].courses[x].personal_income
      tutors[i].class_count = tutors[i].class_count + tutors[i].courses[x].classes.length
    }
  }
  res.json(tutors)
}

exports.doGetTutorQualificationsValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doGetTutorQualifications = async function (req, res) {
  var qualifications = await Qualification.find(req.query)
  res.json(qualifications)
}

exports.toggolTutorQualificationValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.toggolTutorQualification = async function (req, res) {
  console.log(`qualifiying ${req.query._id}`)
  var tutor = await User.findOne({ _id: req.query._id })
  tutor.is_approved = req.query.qualified
  await tutor.save()
  res.json({ success: true })
}

/* exports.doApproveTutor = async function (req, res) {
  var tutor = await User.findOne({ _id: req.query._id })
  tutor.is_approved = true
  await tutor.save()
  res.json({ success: true })
}

exports.doRevokeTutor = async function (req, res) {
  var tutor = await User.findOne({ _id: req.query._id })
  tutor.is_approved = false
  await tutor.save()
  res.json({ success: true })
} */

exports.doGetCoursesValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doGetCourses = function (req, res) {
  Course.find({}, function (err, result) {
    if (err) throw err
    console.log()
    res.json(result)
  })
}

exports.toggoleCourseAprovalValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.toggoleCourseAproval = async function (req, res) {
  var course = await Course.findOne({ _id: req.query._id })
  course.is_approved = !course.is_approved
  await course.save()
  res.json({ success: true })
}

exports.doGetPaymentRecordsValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}

exports.doGetPaymentRecords = function (req, res) {
  res.json({})
}

exports.doGetPaymentsStatsValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doGetPaymentsStats = function (req, res) {
  res.json({})
}

exports.doGetPageContentValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doGetPageContent = function (req, res) {
  res.json({})
}

exports.doUpdatePageContentValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doUpdatePageContent = function (req, res) {
  res.json({})
}

exports.doGetSmsRecordsValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doGetSmsRecords = function (req, res) {
  res.json({})
}

exports.doGetSmsStatsValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doGetSmsStats = function (req, res) {
  res.json({})
}

exports.doGetWechatSettingsValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doGetWechatSettings = function (req, res) {
  res.json({})
}

exports.doSaveWechatSettingsValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doSaveWechatSettings = function (req, res) {
  res.json({})
}

exports.doGetWechatMenuValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doGetWechatMenu = function (req, res) {
  res.json({})
}

exports.doSaveWechatMenuValidation = {
  headers: Joi.object(),
  params: Joi.object(),
  query: Joi.object(),
  cookies: Joi.object(),
  signedCookies: Joi.object(),
  body: Joi.object(),
}
exports.doSaveWechatMenu = function (req, res) {
  res.json({})
}
