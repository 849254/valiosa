const paymentMethod = require('../models/paymentMethod.model'),
    { Joi } = require('express-validation')

exports.getAllPaymentsMethodsValidation = {
        headers: Joi.object(),
        params: Joi.object(),
        query: Joi.object(),
        cookies: Joi.object(),
        signedCookies: Joi.object(),
        body: Joi.object(),
    }
exports.getAllPaymentsMethods = async function (req, res) {
        var paymentsMethods = {}
        res.json({ success: true, paymentsMethods: paymentsMethods })
    }
    
exports.getPaymentMethodValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.getPaymentMethod = async function (req, res) {
    var paymentMethod = {}
    res.json({ success: true, paymentMethod: paymentMethod })
}

exports.addPaymentMethodValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.addPaymentMethod = async function (req, res) {
    var paymentMethod = {}
    res.json({ success: true })
}

exports.updatePaymentMethodValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.updatePaymentMethod = async function (req, res) {
    var paymentMethod = {}
    res.json({ success: true })
}

exports.deletePaymentMethodValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.deletePaymentMethod = async function (req, res) {
    var paymentMethod = {}
    res.json({ success: true })
}