const order = require('../models/Order.model'),
    { Joi } = require('express-validation')

exports.getAllOrdersValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.getAllOrders = async function (req, res) {
    var orders = {}
    res.json({ success: true, orders: orders })
}

exports.getOrderValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.getOrder = async function (req, res) {
    var orders = {}
    res.json({ success: true, orders: orders })
}

exports.addOrderValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.addOrder = async function (req, res) {
    var orders = {}
    res.json({ success: true })
}

exports.updateOrderValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.updateOrder = async function (req, res) {
    var orders = {}
    res.json({ success: true })
}

exports.deleteOrderValidation = {
    headers: Joi.object(),
    params: Joi.object(),
    query: Joi.object(),
    cookies: Joi.object(),
    signedCookies: Joi.object(),
    body: Joi.object(),
}
exports.deleteOrder = async function (req, res) {
    var orders = {}
    res.json({ success: true })
}