const mongoose = require('mongoose')
const contentSchema = new mongoose.Schema({
    title: { type: String },
    content: { type: String },
    others: { type: Object },

    likesUserId: { type: String },
    lovesUserId: { type: String },
    dislikeUserId: { type: String },

    postId: { type: String },
    userId: { type: String },
    commentId: { type: String },

    createdAt: { type: Date, default: Date.now() },
    updatedAt: { type: Date, default: Date.now() },
    createdById: { type: String, null: false, default: 'System' },
    updatedById: { type: String, null: false, default: 'System' }
})
contentSchema.statics.updateAboutIntro = async function (params) {
    await this.updateOne({ name: 'intro' }, {
        obj: {
            Content: params.intro,
            URL: params.url
        }
    })
}
contentSchema.statics.updatePrivacyPolicy = async function (params) {
    await this.updateOne({ name: 'privacyPolicy' }, { obj: { privacyPolicy: params } })
}

contentSchema.statics.updateTermsOfService = async function (params) {
    await this.updateOne({ name: 'termsOfService' }, {
        obj: { termsOfService: params }
    })
}

contentSchema.statics.updateMileStones = async function (params) {
    await this.updateOne({ name: 'milestones' }, { obj: { Array: JSON.parse(JSON.stringify(params)) } })
    console.log(`the params are ${JSON.stringify(params)}`)
}

contentSchema.statics.updateTeamMembers = async function (params) {
    await this.updateOne({ name: 'team' }, { obj: { Team: JSON.parse(JSON.stringify(params)) } })
    console.log(`the params are ${JSON.stringify(params)}`)
}
contentSchema.statics.doLog = async function (params) {
    var logs = await this.findOne({ name: 'logs' })
    logs = logs.obj.logs
    logs.push(params)
    await this.updateOne({ name: 'logs' }, { obj: { logs: logs } })
}

contentSchema.statics.doGetLogs = async function (params) {
    var logs = await this.findOne({ name: 'logs' })
    return logs.obj.logs
}

contentSchema.statics.saveSlides = async function (params) {
    await this.updateOne({ name: 'caroselSlides' }, {
        obj: { caroselSlides: params.slides }
    })
}

module.exports = mongoose.model('Comment', contentSchema)
