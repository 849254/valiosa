const mongoose = require('mongoose'),
  Schema = mongoose.Schema
// eslint-disable-next-line no-unused-vars
var success, msg

const notificationSchema = new Schema({
  isSeen: { type: String },
  isDeleted: { type: String },
  description: { type: String },
  content: { type: Array },
  icon: { type: Array },
  others: { type: Object },

  userId: { type: String },
  createdAt: { type: Date, default: Date.now() },
  updatedAt: { type: Date, default: Date.now() },
  createdById: { type: String, null: false, default: 'System' },
  updatedById: { type: String, null: false, default: 'System' }
})

notificationSchema.statics.getnotification = async function (params) {
  var notification = await this.find(params)
  return notification
}

notificationSchema.statics.addUser = async function (params) {
  var user = await this.create(params)
  await user.save()
  return true
}

notificationSchema.statics.getUser = async function (userId) {
  var user = await this.findOne({ _id: userId })
  return user
}

notificationSchema.statics.updateUser = async function (params) {
  var id = params._id
  delete params._id
  await this.updateOne({ _id: id }, { $set: params })
  return true
}

notificationSchema.statics.deleteUser = async function (userId) {
  var msg = await this.deleteOne(userId)
  console.log(`deleted ${JSON.stringify(msg)}`)
  return true
}

const notificationModel = mongoose.model('notification', notificationSchema)

module.exports = notificationModel
