const mongoose = require('mongoose'),
    Schema = mongoose.Schema
// eslint-disable-next-line no-unused-vars
var success, msg

const postCategorySchema = new Schema({
    name: { type: String },
    visible: { type: Boolean, default: true },
    others: { type: Object },

    userId: { type: String },

    createdAt: { type: Date, default: Date.now() },
    updatedAt: { type: Date, default: Date.now() },
    createdById: { type: String, null: false, default: 'System' },
    updatedById: { type: String, null: false, default: 'System' }
})

postCategorySchema.statics.getMyUsers = async function (params) {
    var users = await this.find(params)
    return users
}

postCategorySchema.statics.addUser = async function (params) {
    var user = await this.create(params)
    await user.save()
    return true
}

postCategorySchema.statics.getUser = async function (userId) {
    var user = await this.findOne({ _id: userId })
    return user
}

postCategorySchema.statics.updateUser = async function (params) {
    var id = params._id
    delete params._id
    await this.updateOne({ _id: id }, { $set: params })
    return true
}

postCategorySchema.statics.deleteUser = async function (userId) {
    var msg = await this.deleteOne(userId)
    console.log(`deleted ${JSON.stringify(msg)}`)
    return true
}

const postCategoryModel = mongoose.model('PostCategory', postCategorySchema)

module.exports = postCategoryModel
