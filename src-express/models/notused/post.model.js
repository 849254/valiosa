const mongoose = require('mongoose'),
    Schema = mongoose.Schema
// eslint-disable-next-line no-unused-vars
var success, msg

const postSchema = new Schema({
    title: { type: String },
    abstract: { type: String },
    content: { type: String },
    visible: { type: Boolean, default: true },
    imgUrl: { type: String },
    date: { type: Date, default: Date.now() },
    others: { type: Object },

    userId: { type: String },
    postCategoryId: { type: String },

    createdAt: { type: Date, default: Date.now() },
    updatedAt: { type: Date, default: Date.now() },
    createdById: { type: String, null: false, default: 'System' },
    updatedById: { type: String, null: false, default: 'System' }
})

postSchema.statics.getMyUsers = async function (params) {
    var users = await this.find(params)
    return users
}

postSchema.statics.addUser = async function (params) {
    var user = await this.create(params)
    await user.save()
    return true
}

postSchema.statics.getUser = async function (userId) {
    var user = await this.findOne({ _id: userId })
    return user
}

postSchema.statics.updateUser = async function (params) {
    var id = params._id
    delete params._id
    await this.updateOne({ _id: id }, { $set: params })
    return true
}

postSchema.statics.deleteUser = async function (userId) {
    var msg = await this.deleteOne(userId)
    console.log(`deleted ${JSON.stringify(msg)}`)
    return true
}

const postModel = mongoose.model('Post', postSchema)

module.exports = postModel
