const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  phoneToken = require('generate-sms-verification-code')

var success, msg

const phoneVerificationSchema = new Schema({
  phone_number: { type: String },
  codes: { type: Array, default: ['xxxxxxxxxxxx'] }
})

phoneVerificationSchema.statics.generateCode = async function (phoneNumber) {
  var code = ''
  if (process.platform === 'linux') {
    code = phoneToken(6, { type: 'number' })
  } else {
    code = '123456'
  }
  var verify = await this.create({ phone_number: phoneNumber })
  verify.codes.push(code)
  await verify.save()
  return code
}

phoneVerificationSchema.statics.verifCode = async function (code, phoneNumber) {
  var verification = this.findOne({ codes: code, phone_number: phoneNumber })
  success = false
  msg = '失败'
  if (verification != null) {
    success = true
    msg = '成功'
    this.deleteOne({ codes: code, phone_number: phoneNumber })
  }
  return { success: success, msg: msg }
}

const phoneVerificationModel = mongoose.model('PhoneVerification', phoneVerificationSchema)

module.exports = phoneVerificationModel
