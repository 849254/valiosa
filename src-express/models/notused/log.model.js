const mongoose = require('mongoose'),
  Schema = mongoose.Schema
// eslint-disable-next-line no-unused-vars
var success, msg

const logSchema = new Schema({
  name: { type: String },
  type: { type: Number },
  others: { type: Object },

  userId: { type: Array },
  clientId: { type: Date },
  flightId: { type: Number },

  createdAt: { type: Date, default: Date.now() },
  updatedAt: { type: Date, default: Date.now() },
  createdById: { type: String, null: false, default: 'System' },
  updatedById: { type: String, null: false, default: 'System' }
})

logSchema.statics.getMylogs = async function (params) {
  var logs = await this.find(params)
  return logs
}

logSchema.statics.addlog = async function (params) {
  var log = await this.create(params)
  await log.save()
  return true
}

logSchema.statics.getlog = async function (logId) {
  var log = await this.findOne({ _id: logId })
  return log
}

logSchema.statics.updatelog = async function (params) {
  var id = params._id
  delete params._id
  await this.updateOne({ _id: id }, { $set: params })
  return true
}

logSchema.statics.deletelog = async function (logId) {
  var msg = await this.deleteOne(logId)
  console.log(`deleted ${JSON.stringify(msg)}`)
  return true
}

const logModel = mongoose.model('Logs', logSchema)

module.exports = logModel
