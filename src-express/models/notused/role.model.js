const mongoose = require('mongoose'),
  Schema = mongoose.Schema
// eslint-disable-next-line no-unused-vars
var success, msg

const roleSchema = new Schema({
  name: { type: String, minlength: 5, unique: true, allowNull: false,},
  description: { type: String, minlength: 5, allowNull: false },
  others: { type: Object },

  createdAt: { type: Date, default: Date.now() },
  updatedAt: { type: Date, default: Date.now() },
  createdById: { type: String, null: false, default: 'System' },
  updatedById: { type: String, null: false, default: 'System' }
})

roleSchema.statics.getMyRoles = async function (params) {
  var roles = await this.find(params)
  return roles
}

roleSchema.statics.addRole = async function (params) {
  var role = await this.create(params)
  await role.save()
  return true
}

roleSchema.statics.getRole = async function (roleId) {
  var role = await this.findOne({ _id: roleId })
  return role
}

roleSchema.statics.updateRole = async function (params) {
  var id = params._id
  delete params._id
  await this.updateOne({ _id: id }, { $set: params })
  return true
}

roleSchema.statics.deleteRole = async function (roleId) {
  var msg = await this.deleteOne(roleId)
  console.log(`deleted ${JSON.stringify(msg)}`)
  return true
}

const roleModel = mongoose.model('Role', roleSchema)

module.exports = roleModel
