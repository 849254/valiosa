const mongoose = require('mongoose'),
  Schema = mongoose.Schema
// eslint-disable-next-line no-unused-vars
var success, msg

const permissionSchema = new Schema({
  name: { type: String, enum:['full_administration','product_managment','category_managment','personal_settings'] },
  description: { type: String },
  others: { type: Object },


  createdAt: { type: Date, default: Date.now() },
  updatedAt: { type: Date, default: Date.now() },
  createdById: { type: String, null: false, default: 'System' },
  updatedById: { type: String, null: false, default: 'System' }
})

permissionSchema.statics.getMyPermissions = async function (params) {
  var permissions = await this.find(params)
  return permissions
}

permissionSchema.statics.addPermission = async function (params) {
  var permission = await this.create(params)
  await permission.save()
  return true
}

permissionSchema.statics.getPermission = async function (permissionId) {
  var permission = await this.findOne({ _id: permissionId })
  return permission
}

permissionSchema.statics.updatePermission = async function (params) {
  var id = params._id
  delete params._id
  await this.updateOne({ _id: id }, { $set: params })
  return true
}

permissionSchema.statics.deletePermission = async function (permissionId) {
  var msg = await this.deleteOne(permissionId)
  console.log(`deleted ${JSON.stringify(msg)}`)
  return true
}

const permissionModel = mongoose.model('Permission', permissionSchema)

module.exports = permissionModel
